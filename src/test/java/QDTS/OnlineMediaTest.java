
package QDTS;

import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for Exercise5.
 */

@DisplayName("5th exercise Test")
public class OnlineMediaTest {
    private final static PipedOutputStream pipe = new PipedOutputStream();
    private final static ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final static ByteArrayOutputStream err = new ByteArrayOutputStream();

    private final static InputStream originalIn = System.in;
    private final static PrintStream originalOut = System.out;
    private final static PrintStream originalErr = System.err;

    @BeforeAll
    public static void catchIO() throws IOException {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
        System.setIn(new PipedInputStream(pipe));
    }

    @AfterAll
    public static void releaseIO() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setIn(originalIn);
    }


    @Test
    @DisplayName("5th exercise Test")
    public void OnlineMediaTestMain() throws Exception {

        OnlineMedia.main(new String[0]);
        String[] output = out.toString().split(System.lineSeparator());
        assertEquals("dvd1.title = IBM Dance Party", output[0]);
        assertEquals("dvd2.title = IBM Kids Sing-along", output[1]);
        assertEquals("dvd3.title = IBM Smarter Planet", output[2]);
        assertEquals("Book title = Java Programming", output[3]);
        assertEquals("Total Cost of the Order is: 133.88", output[4]);
    }
}
